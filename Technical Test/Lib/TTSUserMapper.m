//
//  TTSUserMapper.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 06/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUserMapper.h"
#import <ISO8601/ISO8601.h>

#define VALUEIFNOTNULL(a) a != [NSNull null] ? a : nil

@implementation TTSUserMapper

+ (instancetype)sharedInstance {
    static TTSUserMapper * sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TTSUserMapper alloc] init];
    });
    
    return sharedInstance;
}

- (TTSUser * _Nullable)mapUserJSON:(NSDictionary<NSString *, id> * _Nonnull)json {
    
    TTSGender gender = Unknown;
    NSString * genderString = VALUEIFNOTNULL(json[@"gender"]);
    if ([genderString isEqual:@"male"]) {
        gender = Male;
    } else if ([genderString isEqual:@"female"]) {
        gender = Female;
    }
    
    NSString * firstName = VALUEIFNOTNULL([json valueForKeyPath:@"name.first"]);
    if (!firstName) {
        return nil;
    }
    
    NSString * lastName = VALUEIFNOTNULL([json valueForKeyPath:@"name.last" ]);
    if (!lastName) {
        return nil;
    }
    
    NSString * email = VALUEIFNOTNULL(json[@"email" ]);
    if (!email) {
        return nil;
    }
    
    NSString * birthdateString = VALUEIFNOTNULL(json[@"dob"]);
    if (!birthdateString) {
        return nil;
    }
    NSDate * birthdate = [NSDate dateWithISO8601String:birthdateString];
    if (!birthdate) {
        return nil;
    }
    
    NSString * profilePictureBigString = VALUEIFNOTNULL([json valueForKeyPath:@"picture.large"]);
    if (!profilePictureBigString) {
        return nil;
    }
    NSURL * profilePictureBig = [NSURL URLWithString:profilePictureBigString];
    if (!profilePictureBig) {
        return nil;
    }
    
    NSString * profilePictureMediumString = VALUEIFNOTNULL([json valueForKeyPath:@"picture.medium"]);
    if (!profilePictureMediumString) {
        return nil;
    }
    NSURL * profilePictureMedium = [NSURL URLWithString:profilePictureMediumString];
    if (!profilePictureMedium) {
        return nil;
    }
    
    NSString * profilePictureThumbString = VALUEIFNOTNULL([json valueForKeyPath:@"picture.thumbnail"]);
    if (!profilePictureThumbString) {
        return nil;
    }
    NSURL * profilePictureThumb = [NSURL URLWithString:profilePictureThumbString];
    if (!profilePictureThumb) {
        return nil;
    }
    
    return [[TTSUser alloc] initWithGender:gender
                                 firstName:firstName
                                  lastName:lastName
                                     email:email
                                 birthdate:birthdate
                       profilePictureLarge:profilePictureBig
                      profilePictureMedium:profilePictureMedium
                       profilePictureThumb:profilePictureThumb];
}

- (NSArray<TTSUser *> * _Nullable)mapUsersJSON:(NSArray<NSDictionary<NSString *, id> *> * _Nonnull)json {
    
    NSMutableArray *array = [NSMutableArray array];
    [json enumerateObjectsUsingBlock:^(NSDictionary<NSString *,id> * _Nonnull jsonDict, NSUInteger idx, BOOL * _Nonnull stop) {
        TTSUser * user = [self mapUserJSON:jsonDict];
        if (user) {
            [array addObject:user];
        }
    }];
    
    return [array copy];
}

@end
