//
//  TTSUserMapper.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 06/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTSUser.h"

/**
 TTSUserMapper is a JSON -> TTSUser mapper. It can be used by the User Service
 to map API responses (JSON) to domain models (`TTSUser`).
 */
@interface TTSUserMapper : NSObject

/**
 A shared instance of the mapper. You can instantiate your own mapper, this is
 just an utility.
 */
+ (instancetype _Nullable)sharedInstance;

/**
 Maps the JSON to hopefully create a single `TTSUser`.

 @return The optional domain user.
 */
- (TTSUser * _Nullable)mapUserJSON:(NSDictionary<NSString *, id> * _Nonnull)json;

/**
 Maps the JSON to hopefully create an array of `TTSUser`s.
 
 @return The optional domain user.
 */
- (NSArray<TTSUser *> * _Nullable)mapUsersJSON:(NSArray<NSDictionary<NSString *, id> *> * _Nonnull)json;

@end
