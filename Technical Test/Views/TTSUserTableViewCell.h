//
//  TTSUserTableViewCell.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTSUserPresenter.h"

/**
 The TTSUserTableViewCell is a simple Table View cell subclass that can display
 information about an user, using the `TTSUserPresenter`.
 */
@interface TTSUserTableViewCell : UITableViewCell<TTSUserPresenterTargetView>

/**
 The presenter used to display information about an user.
 */
@property (strong, nullable, nonatomic) TTSUserPresenter * presenter;

@end
