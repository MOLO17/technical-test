//
//  TTSUserCollectionViewCell.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 06/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSUserPresenter.h"

/**
 The TTSUserCollectionViewCell is a simple Collection View Cell subclass that
 can display information about an user, using the `TTSUserPresenter`.
 */
@interface TTSUserCollectionViewCell : UICollectionViewCell<TTSUserPresenterTargetView>

/**
 The presenter used to display information about an user.
 */
@property (strong, nullable, nonatomic) TTSUserPresenter * presenter;

@end

