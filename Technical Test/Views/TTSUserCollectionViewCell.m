//
//  TTSUserCollectionViewCell.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 06/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUserCollectionViewCell.h"
#import <PureLayout/PureLayout.h>

@interface TTSUserCollectionViewCell ()

/**
 The image view displaying the profile picture of the user.
 */
@property (strong, nonnull, nonatomic) UIImageView *thumbnailImageView;

/**
 The label displaying the full name of the user.
 */
@property (strong, nonnull, nonatomic) UILabel *nameLabel;

/**
 The label displaying the email of the user.
 */
@property (strong, nonnull, nonatomic) UILabel *emailLabel;

@end

@implementation TTSUserCollectionViewCell

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    self.backgroundColor = [UIColor whiteColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    _thumbnailImageView = [self makeProfilePictureImageView];
    _nameLabel = [self makeTextLabel];
    _emailLabel = [self makeTextLabel];
    
    [self.contentView addSubview:self.thumbnailImageView];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.emailLabel];
    
    [self.thumbnailImageView autoMatchDimension:ALDimensionWidth
                                    toDimension:ALDimensionWidth
                                         ofView:self.contentView
                                 withMultiplier:0.6];
    [self.thumbnailImageView autoMatchDimension:ALDimensionHeight
                                    toDimension:ALDimensionWidth
                                         ofView:self.thumbnailImageView];
    [self.thumbnailImageView autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    [self.nameLabel autoPinEdge:ALEdgeTop
                         toEdge:ALEdgeBottom
                         ofView:self.thumbnailImageView
                     withOffset:8];
    [self.nameLabel autoPinEdgeToSuperviewMargin:ALEdgeLeading];
    [self.nameLabel autoPinEdgeToSuperviewMargin:ALEdgeTrailing];
    
    [self.emailLabel autoPinEdge:ALEdgeTop
                          toEdge:ALEdgeBottom
                          ofView:self.nameLabel
                      withOffset:4];
    [self.emailLabel autoPinEdgeToSuperviewMargin:ALEdgeLeading];
    [self.emailLabel autoPinEdgeToSuperviewMargin:ALEdgeTrailing];
    
    return self;
}

#pragma mark - Setters

- (void)setPresenter:(TTSUserPresenter *)presenter {
    _presenter = presenter;
    
    if (!presenter) {
        return;
    }
    
    presenter.view = self;
}

#pragma mark - UITableViewCell overrides

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.presenter = nil;
    
    [self showFullName:@""];
    [self showEmail:@""];
    [self showProfilePicture:nil];
}

#pragma mark - Factories

- (UIImageView *)makeProfilePictureImageView {
    return [UIImageView newAutoLayoutView];
}

- (UILabel *)makeTextLabel {
    UILabel * l = [UILabel newAutoLayoutView];
    l.textAlignment = NSTextAlignmentCenter;
    
    return l;
}


#pragma mark - TTSUserPresenterTargetView

- (void)showFullName:(NSString *)fullName {
    self.nameLabel.text = fullName;
}

- (void)showEmail:(NSString *)email {
    self.emailLabel.text = email;
}

- (void)showProfilePicture:(UIImage *)profilePicture {
    self.thumbnailImageView.image = profilePicture;
    [self setNeedsLayout];
}

@end
