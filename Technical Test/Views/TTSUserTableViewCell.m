//
//  TTSUserTableViewCell.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUserTableViewCell.h"

@implementation TTSUserTableViewCell

#pragma mark - Setters

- (void)setPresenter:(TTSUserPresenter *)presenter {
    _presenter = presenter;
    
    if (!presenter) {
        return;
    }
    
    presenter.view = self;
}

#pragma mark - UITableViewCell overrides

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.presenter = nil;
    
    [self showFullName:@""];
    [self showEmail:@""];
    [self showProfilePicture:nil];
}

#pragma mark - TTSUserPresenterTargetView

- (void)showFullName:(NSString *)fullName {
    self.textLabel.text = fullName;
}

- (void)showEmail:(NSString *)email {
    self.detailTextLabel.text = email;
}

- (void)showProfilePicture:(UIImage *)profilePicture {
    self.imageView.image = profilePicture;
    [self setNeedsLayout];
}

@end
