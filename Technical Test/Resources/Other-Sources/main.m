//
//  main.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 2/5/18
//  Copyright (c) 2018 MOLO17. All rights reserved.
//

@import UIKit;

#import "TTSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TTSAppDelegate class]));
    }
}
