//
//  TTSRootFlowCoordinator.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTSFlowCoordinator.h"
#import "TTSUserService.h"

/**
 TTSRootFlowCoordinator is the main Flow Coordinator, the first one that should
 be instantiated by the AppDelegate. This is the application entry point, from
 the view controller flow point of view.
 */
@interface TTSRootFlowCoordinator : NSObject<TTSFlowCoordinator>

/**
 Initializes a new flow coordinator with the provided dependencies.

 @param service The user service to use to fetch user information.
 @return The instantiated flow coordinator.
 */
- (instancetype _Nullable)initWithUserService:(TTSUserService * _Nonnull)service;

- (instancetype _Nullable)init NS_UNAVAILABLE;

@end
