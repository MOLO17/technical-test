//
//  TTSRootFlowCoordinator.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSRootFlowCoordinator.h"
#import "TTSListKind.h"
#import "TTSListTypeChooserViewController.h"
#import "TTSUserListTableViewController.h"
#import "TTSUserDetailsViewController.h"
#import "TTSUserListCollectionViewController.h"

@interface TTSRootFlowCoordinator ()

@property (strong, nonnull, nonatomic) TTSUserService * service;

/**
 The main navigation controller managed by the flow coordinator.
 */
@property (strong, nullable, nonatomic) UINavigationController * navigationController;

@end

@implementation TTSRootFlowCoordinator

#pragma mark - Init

- (instancetype)initWithUserService:(TTSUserService *)service {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _service = service;
    _navigationController = [[UINavigationController alloc] init];
    
    return self;
}

#pragma mark - Flow Coordinator

- (UIViewController *)initialViewController {
    return self.navigationController;
}

- (void)start {
    [self toListTypeChooser];
}

#pragma mark - Private methods (flow)

/**
 This method navigates to the first scene, where users can choose how they'll be
 displayed the user list, whether using a grid or a simpler list.
 */
- (void)toListTypeChooser {
    
    TTSListTypeChooserViewController *vc = [[TTSListTypeChooserViewController alloc] init];
    
    TTSRootFlowCoordinator * __weak weakSelf = self;
    vc.didChoose = ^ void (TTSListKind choice) {
        
        switch (choice) {
            case TableList:
                [weakSelf toUserListTable];
                break;
                
            case Grid:
                [weakSelf toUserListCollection];
                break;
        }
    };
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)toUserListTable {
    
    TTSUserListPresenter *presenter = [[TTSUserListPresenter alloc] initWithUserService:self.service];
    TTSUserListTableViewController *vc = [[TTSUserListTableViewController alloc] initWithPresenter:presenter];
    
    TTSRootFlowCoordinator * __weak weakSelf = self;
    vc.didSelectUser = ^ void (TTSUser * user) {
        [weakSelf toUserDetail:user];
    };
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)toUserListCollection {
    
    TTSUserListPresenter *presenter = [[TTSUserListPresenter alloc] initWithUserService:self.service];
    TTSUserListCollectionViewController *vc = [[TTSUserListCollectionViewController alloc] initWithPresenter:presenter];
    
    TTSRootFlowCoordinator * __weak weakSelf = self;
    vc.didSelectUser = ^ void (TTSUser * user) {
        [weakSelf toUserDetail:user];
    };
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)toUserDetail:(TTSUser *)user {
    TTSUserPresenter *presenter = [[TTSUserPresenter alloc] initWithUser:user
                                                               imageKind:Large
                                                             userService:self.service];
    TTSUserDetailsViewController *vc = [[TTSUserDetailsViewController alloc] initWithPresenter:presenter];
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
