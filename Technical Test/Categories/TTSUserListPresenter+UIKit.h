//
//  TTSUserListPresenter+UIKit.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTSUserListPresenter.h"

@interface TTSUserListPresenter (UIKitAdditions)

/**
 @param indexPath The index path to the user to retrieve.
 @return The user at the index path, nil if not found.
 */
- (TTSUser * _Nullable)userAtRow:(NSIndexPath * _Nonnull)indexPath;

@end
