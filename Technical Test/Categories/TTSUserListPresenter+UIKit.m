//
//  TTSUserListPresenter+UIKit.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUserListPresenter+UIKit.h"

@implementation TTSUserListPresenter(UIKitAdditions)

- (TTSUser *)userAtRow:(NSIndexPath *)indexPath {
    return self.users[(NSUInteger) indexPath.row];
}

@end
