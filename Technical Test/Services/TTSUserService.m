//
//  TTSUserService.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 06/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUserService.h"

@interface TTSUserService ()

@property (strong, nonnull, nonatomic) AFURLSessionManager * sessionManager;
@property (strong, nonnull, nonatomic) AFHTTPRequestSerializer * HTTPRequestSerializer;
@property (strong, nonnull, nonatomic) TTSUserMapper * mapper;

@end

@implementation TTSUserService

- (instancetype)initWithSessionManager:(AFURLSessionManager *)sessionManager
                     requestSerializer:(AFHTTPRequestSerializer *)requestSerializer
                            userMapper:(TTSUserMapper *)mapper {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _sessionManager = sessionManager;
    _HTTPRequestSerializer = requestSerializer;
    _mapper = mapper;
    
    NSArray * responseSerializers = @[
        [AFJSONResponseSerializer serializer],
        [AFImageResponseSerializer serializer]
    ];
    _sessionManager.responseSerializer = [AFCompoundResponseSerializer compoundSerializerWithResponseSerializers:responseSerializers];
    
    return self;
}

- (NSURLSessionDataTask * _Nullable)requestUserListWithHandler:(void (^_Nonnull)(NSError *, NSArray<TTSUser *> *))handler {
    
    NSError * error;
    NSURLRequest * request = [self.HTTPRequestSerializer requestWithMethod:@"GET"
                                                                 URLString:@"https://randomuser.me/api/?results=100"
                                                                parameters:nil
                                                                     error:&error];
    
    if (error) {
        NSLog(@"failed to create a request to load the user list");
        return nil;
    }
    
    TTSUserService * __weak weakSelf = self;
    NSURLSessionDataTask * task = [self.sessionManager dataTaskWithRequest:request
                           completionHandler:^(NSURLResponse * response, NSDictionary<NSString *, id> * responseObject, NSError * err) {
        if (err) {
            handler(err, nil);
            return;
        }

        NSArray * users = [weakSelf.mapper mapUsersJSON:responseObject[@"results"]];
        handler(nil, users);
    }];
    [task resume];
    
    return task;
}

- (NSURLSessionDataTask *)loadProfilePictureForUser:(TTSUser *)user
                                               kind:(TTSProfilePictureKind)kind
                                        withHandler:(void (^)(NSError *, UIImage *))handler {
    
    NSString * requestString;
    switch (kind) {
        case Large:
            requestString = user.profilePictureLarge.absoluteString;
            break;
            
        case Medium:
            requestString = user.profilePictureMedium.absoluteString;
            break;
            
        case Thumbnail:
            requestString = user.profilePictureThumb.absoluteString;
            break;
    }
    
    NSError * error;
    NSURLRequest * request = [self.HTTPRequestSerializer requestWithMethod:@"GET"
                                                                 URLString:requestString
                                                                parameters:nil
                                                                     error:&error];
    
    if (error) {
        NSLog(@"failed to create a request to load the profile picture");
        return nil;
    }
    
    NSURLSessionDataTask * task = [self.sessionManager dataTaskWithRequest:request
                                                         completionHandler:^(NSURLResponse * response, UIImage * responseObject, NSError * err) {
         if (err) {
             handler(err, nil);
             return;
         }
         
         handler(nil, responseObject);
    }];
    [task resume];
    
    return task;
}

@end
