//
//  TTSUserService.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 06/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "TTSUserMapper.h"

/**
 TTSGeneder defines user gender.
 
 - Large: The biggest profile picture available.
 - Medium: The medium profile picture.
 - Thumbnail: The thumbnail (smallest) profile picture.
 */
typedef NS_ENUM(NSUInteger, TTSProfilePictureKind) {
    Large,
    Medium,
    Thumbnail
};

/**
 The TTSUserService is a nice domain specific gateway to RandomUser.me APIs.
 
 It allows you to have simple methods to perform specific tasks, hiding away
 the API and its requirements/quirks.
 */
@interface TTSUserService : NSObject

/**
 Initializes a new TTSUserService with the provided dependencies.

 @param sessionManager The AFNetworking session manager to use to perform
 requests.
 @param requestSerializer The AFNetworking serializer to use to serialize
 requests.
 @param mapper The user mapper to use to map responses to domain objects.
 @return The initilized User Service.
 */
- (instancetype _Nullable)initWithSessionManager:(AFURLSessionManager * _Nonnull)sessionManager
                               requestSerializer:(AFHTTPRequestSerializer * _Nonnull)requestSerializer
                                      userMapper:(TTSUserMapper * _Nonnull)mapper;

/**
 Fetches the users from the remote API.

 @param handler The handler that will be called providing the result.
 @return The created session data task, you can use this object to cancel
 in-flight requests. The returned task has already been resumed.
 */
- (NSURLSessionDataTask * _Nullable)requestUserListWithHandler:(void (^ _Nonnull)(NSError * _Nullable, NSArray<TTSUser *> * _Nullable))handler;

/**
 Fetches the profile picture image for the provided user.

 @param user The user to load the image of.
 @param kind The kind of profile picture you want to load.
 @param handler The handler that will be called providing the result.
 @return The created session data task, you can use this object to cancel
 in-flight requests. The returned task has already been resumed.
 */
- (NSURLSessionDataTask * _Nullable)loadProfilePictureForUser:(TTSUser * _Nonnull)user
                                                         kind:(TTSProfilePictureKind)kind
                                                  withHandler:(void (^ _Nonnull)(NSError * _Nullable, UIImage * _Nullable))handler;

@end
