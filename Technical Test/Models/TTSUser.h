//
//  TTSUser.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 TTSGeneder defines user gender.
 
 - Male: The user is a male.
 - Female: The user is a female.
 - Unknown: The user didn't provide gender information.
 */
typedef NS_ENUM(NSUInteger, TTSGender) {
    Male,
    Female,
    Unknown
};

/**
 The TTSUser is the representation, in the app's domain, of an user displayed by
 the app.
 */
@interface TTSUser : NSObject

NS_ASSUME_NONNULL_BEGIN
@property (nonatomic) TTSGender gender;
@property (copy, nonnull, readonly, nonatomic) NSString *firstName;
@property (copy, nonnull, readonly, nonatomic) NSString *lastName;
@property (copy, nonnull, readonly, nonatomic) NSString *email;
@property (copy, nonnull, readonly, nonatomic) NSDate *birthdate;
@property (copy, nonnull, readonly, nonatomic) NSURL *profilePictureLarge;
@property (copy, nonnull, readonly, nonatomic) NSURL *profilePictureMedium;
@property (copy, nonnull, readonly, nonatomic) NSURL *profilePictureThumb;
NS_ASSUME_NONNULL_END

/**
 Initializes a new user with the provided information.
 @return The initialized user.
 */
- (instancetype _Nullable)initWithGender:(TTSGender)gender
                     firstName:(NSString * _Nonnull)firstName
                      lastName:(NSString * _Nonnull)lastName
                         email:(NSString * _Nonnull)email
                     birthdate:(NSDate * _Nonnull)birthdate
           profilePictureLarge:(NSURL * _Nonnull)profilePictureLarge
          profilePictureMedium:(NSURL * _Nonnull)profilePictureMedium
           profilePictureThumb:(NSURL * _Nonnull)profilePictureThumb;

- (instancetype _Nullable)init NS_UNAVAILABLE;

@end
