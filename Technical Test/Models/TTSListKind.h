//
//  ListKind.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

/**
 TTSListKind defines the way the user list can be displayed.

 - Table: Display the list using a table.
 - Grid: Display the list in a grid.
 */
typedef NS_ENUM(NSUInteger, TTSListKind) {
    TableList,
    Grid
};
