//
//  TTSUser.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUser.h"

@implementation TTSUser

- (instancetype)initWithGender:(TTSGender)gender
                     firstName:(NSString *)firstName
                      lastName:(NSString *)lastName
                         email:(NSString *)email
                     birthdate:(NSDate *)birthdate
           profilePictureLarge:(NSURL *)profilePictureLarge
          profilePictureMedium:(NSURL *)profilePictureMedium
           profilePictureThumb:(NSURL *)profilePictureThumb {
    
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _gender = gender;
    _firstName = [firstName copy];
    _lastName = [lastName copy];
    _email = [email copy];
    _birthdate = [birthdate copy];
    _profilePictureLarge = [profilePictureLarge copy];
    _profilePictureMedium = [profilePictureMedium copy];
    _profilePictureThumb = [profilePictureThumb copy];
    
    return self;
}

@end
