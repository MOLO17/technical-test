//
//  TTSUserListPresenter.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTSUser.h"
#import "TTSUserService.h"

/**
 The TTSUserListPresenterTargetView defines the methods that views (target)
 should expose in order to be completely functional. Implementing these method
 allows a Presenter to drive a view.
 */
@protocol TTSUserListPresenterTargetView

- (void)displayUsers:(NSArray<TTSUser *> * _Nonnull)users;

@end

/**
 The TTSUserListPresenter drives a view in order to abstract common business
 logic from the view. Using this presenter, the view is expected to display
 informatio about a list of users.
 */
@interface TTSUserListPresenter : NSObject

/**
 The list of users currently displayed by the view controller.
 */
@property (copy, nullable, readonly, nonatomic) NSArray<TTSUser *> * users;

/**
 The service used to perform requests.
 */
@property (strong, nullable, readonly, nonatomic) TTSUserService *service;

/**
 The view that's currently driven by the presenter. Setting the view will make
 the presenter fetch the data.
 */
@property (weak, nullable, nonatomic) id<TTSUserListPresenterTargetView> view;

/**
 Initializes a new presenter with the provided service.

 @param service The service that will be used by the presenter to load the user
 list.
 @return The initialized presenter.
 */
- (instancetype _Nullable)initWithUserService:(TTSUserService * _Nonnull)service;

- (instancetype _Nullable)init NS_UNAVAILABLE;

@end
