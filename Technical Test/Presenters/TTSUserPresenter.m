//
//  TTSUserPresenter.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUserPresenter.h"

@interface TTSUserPresenter ()

// Make writeable
@property (copy, nullable, readwrite, nonatomic) TTSUser * user;

/**
 The kind of profile picture that will be loaded.
 */
@property (nonatomic) TTSProfilePictureKind targetProfilePictureKind;

/**
 The service used to perform requests.
 */
@property (strong, nullable, nonatomic) TTSUserService *service;

/**
 The current, in-flight, request.
 */
@property (strong, nullable, nonatomic) NSURLSessionTask *currentRequest;

@end

@implementation TTSUserPresenter

#pragma mark - Init

- (instancetype)initWithUser:(TTSUser *)user
                   imageKind:(TTSProfilePictureKind)targetProfilePicture
                 userService:(TTSUserService *)service {

    self = [super init];
    if (!self) {
        return nil;
    }
    
    _user = user;
    _targetProfilePictureKind = targetProfilePicture;
    _service = service;
    
    return self;
}

- (instancetype)init NS_UNAVAILABLE {
    return nil;
}

#pragma mark - Setters

- (void)setView:(id<TTSUserPresenterTargetView>)view {
    _view = view;
    
    [self.currentRequest cancel];
    
    if (!view) {
        return;
    }
    
    NSString * fullName = [NSString stringWithFormat:@"%@ %@", self.user.firstName, self.user.lastName];
    [view showFullName:fullName];
    [view showEmail:self.user.email];
    
    self.currentRequest = [self.service loadProfilePictureForUser:self.user
                                                             kind:self.targetProfilePictureKind
                                                      withHandler:^(NSError * error, UIImage * image) {
        if (image) {
            [view showProfilePicture:image];
        }
    }];
}

@end
