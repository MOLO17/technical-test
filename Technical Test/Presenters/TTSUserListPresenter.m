//
//  TTSUserListPresenter.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUserListPresenter.h"

@interface TTSUserListPresenter ()

// Make writeable
@property (copy, nullable, readwrite, nonatomic) NSArray<TTSUser *> * users;

/**
 The current, in-flight, request.
 */
@property (strong, nullable, nonatomic) NSURLSessionTask *currentRequest;

@end

@implementation TTSUserListPresenter

#pragma mark - Init

- (instancetype)initWithUserService:(TTSUserService * _Nonnull)service {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _service = service;
    
    return self;
}

#pragma mark - Setters

- (void)setView:(id<TTSUserListPresenterTargetView>)view {
    _view = view;
    
    [self.currentRequest cancel];
    
    if (!view) {
        return;
    }
    
    TTSUserListPresenter * __weak weakSelf = self;
    self.currentRequest = [self.service requestUserListWithHandler:^(NSError * error, NSArray<TTSUser *> * users) {
        
        if (error) {
            NSLog(@"Loading the user list failed with error: %@", error);
            return;
        }
        
        TTSUserListPresenter * strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        strongSelf.users = users;
        [strongSelf.view displayUsers:users];
        strongSelf.currentRequest = nil;
        
    }];

}

@end
