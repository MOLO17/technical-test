//
//  TTSUserPresenter.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTSUser.h"
#import "TTSUserService.h"

/**
 The TTSUserPresenterTargetView defines the methods that views (target)
 should expose in order to be completely functional. Implementing these method
 allows a Presenter to drive a view.
 */
@protocol TTSUserPresenterTargetView

/**
 Update the view displaying the new full name for the user.

 @param fullName The user's full name.
 */
- (void)showFullName:(NSString * _Nonnull)fullName;

/**
 Update the view displaying the new email for the user.
 
 @param email The user's email.
 */
- (void)showEmail:(NSString * _Nonnull)email;

/**
 Update the view displaying the new profile picture image for the user.
 
 @param profilePicture The user's profile picture.
 */
- (void)showProfilePicture:(UIImage * _Nullable)profilePicture;

@end

/**
 The TTSUserPresenter drives a view in order to abstract common business
 logic from the view. Using this presenter, the view is expected to display
 informatio about a single user.
 */
@interface TTSUserPresenter : NSObject

/**
 The user currently displayed
 */
@property (copy, nonnull, readonly, nonatomic) TTSUser * users;

/**
 The view that's currently driven by the presenter.
 */
@property (weak, nullable, nonatomic) id<TTSUserPresenterTargetView> view;

/**
 Initializes a new presenter with the provided information.
 
 @param user The user to gather information from.
 @param targetProfilePicture The kind of profile picture the presenter will load.
 @param service The service that will be used by the presenter to load the user
 list.
 @return The initialized presenter.
 */
- (instancetype _Nullable)initWithUser:(TTSUser * _Nonnull)user
                             imageKind:(TTSProfilePictureKind)targetProfilePicture
                           userService:(TTSUserService * _Nonnull)service;

- (instancetype _Nullable)init NS_UNAVAILABLE;

@end

