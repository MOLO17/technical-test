//
//  TTSFlowCoordinator.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The TTSFlowCoordinator protocol defines the common interface for Flow
 Coordinators. A Flow Coordinator helps reducing the the interactions between
 View Controllers, allowing for higher segragation, thus reducing coupling.
 */
@protocol TTSFlowCoordinator <NSObject>

/**
 @return The initial view controller for the scene managed by the Flow
 Coordinator.
 */
- (UIViewController *)initialViewController;

/**
 Starts the scene.
 */
- (void)start;

@end
