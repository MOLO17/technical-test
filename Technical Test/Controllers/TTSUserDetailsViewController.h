//
//  TTSUserDetailsViewController.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSUserPresenter.h"

/**
 The TTSUserDetailsViewController is a View Controller that can display
 information about a single user.
 */
@interface TTSUserDetailsViewController : UIViewController

/**
 Initializes a new view controller.
 
 @param presenter The presenter the view controller should use.
 @return The instantiated view controller.
 */
- (instancetype _Nullable)initWithPresenter:(TTSUserPresenter * _Nonnull)presenter NS_DESIGNATED_INITIALIZER;

@end
