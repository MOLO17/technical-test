//
//  TTSListTypeChooserViewController.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSListKind.h"

/**
 The TTSListTypeChooserViewController is a simple view controller that allows
 users to pick and choose how the user list will be displayed.
 */
@interface TTSListTypeChooserViewController : UIViewController

/**
 A block that's called once users performed their decision. It provides their
 choice.
 */
@property (copy, nullable, nonatomic) void (^didChoose)(TTSListKind);

@end
