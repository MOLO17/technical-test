//
//  TTSUserDetailsViewController.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSUserDetailsViewController.h"
#import <PureLayout/PureLayout.h>

@interface TTSUserDetailsViewController () <TTSUserPresenterTargetView>

/**
 The presenter that drives the view controller.
 */
@property (strong, nonnull, nonatomic) TTSUserPresenter * presenter;

/**
 Whether we already pinned the view to the top layout guide.
 */
@property (nonatomic) BOOL didPinToTopLayoutGuide;

/**
 The image view displaying the profile picture of the user.
 */
@property (strong, nonnull, nonatomic) UIImageView *profilePictureImageView;

/**
 The label displaying the full name of the user.
 */
@property (strong, nonnull, nonatomic) UILabel *fullNameLabel;

/**
 The label displaying the email of the user.
 */
@property (strong, nonnull, nonatomic) UILabel *emailLabel;

@end

@implementation TTSUserDetailsViewController

#pragma mark - Init

- (instancetype)initWithPresenter:(TTSUserPresenter *)presenter {
    self = [super initWithNibName:nil bundle:nil];
    if (!self) {
        return nil;
    }
    
    _presenter = presenter;
    
    _didPinToTopLayoutGuide = false;
    _profilePictureImageView = [self makeProfilePictureImageView];
    _fullNameLabel = [self makeTextLabel];
    _emailLabel = [self makeTextLabel];
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE {
    return nil;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE {
    return nil;
}

#pragma mark - View lifecycle

- (void)loadView {
    self.view = [[UIView alloc] init];
    
    [self.view addSubview:self.profilePictureImageView];
    [self.view addSubview:self.fullNameLabel];
    [self.view addSubview:self.emailLabel];
    
    [self.profilePictureImageView autoMatchDimension:ALDimensionWidth
                                         toDimension:ALDimensionWidth
                                              ofView:self.view
                                      withMultiplier:0.6];
    [self.profilePictureImageView autoMatchDimension:ALDimensionHeight
                                         toDimension:ALDimensionWidth
                                              ofView:self.profilePictureImageView];
    [self.profilePictureImageView autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    [self.fullNameLabel autoPinEdge:ALEdgeTop
                             toEdge:ALEdgeBottom
                             ofView:self.profilePictureImageView
                         withOffset:16];
    [self.fullNameLabel autoPinEdgeToSuperviewMargin:ALEdgeLeading];
    [self.fullNameLabel autoPinEdgeToSuperviewMargin:ALEdgeTrailing];

    [self.emailLabel autoPinEdge:ALEdgeTop
                          toEdge:ALEdgeBottom
                          ofView:self.fullNameLabel
                      withOffset:16];
    [self.emailLabel autoPinEdgeToSuperviewMargin:ALEdgeLeading];
    [self.emailLabel autoPinEdgeToSuperviewMargin:ALEdgeTrailing];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.presenter.view = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!self.didPinToTopLayoutGuide) {
        [self.profilePictureImageView autoPinToTopLayoutGuideOfViewController:self
                                                                    withInset:16];
    }
}

#pragma mark - Factories

- (UIImageView *)makeProfilePictureImageView {
    return [UIImageView newAutoLayoutView];
}

- (UILabel *)makeTextLabel {
    UILabel * l = [UILabel newAutoLayoutView];
    l.textAlignment = NSTextAlignmentCenter;
    
    return l;
}

#pragma mark - TTUSerPresenterTargetView

- (void)showFullName:(NSString * _Nonnull)fullName {
    self.fullNameLabel.text = fullName;
}

- (void)showEmail:(NSString * _Nonnull)email {
    self.emailLabel.text = email;
}

- (void)showProfilePicture:(UIImage * _Nullable)profilePicture {
    self.profilePictureImageView.image = profilePicture;
}

@end
