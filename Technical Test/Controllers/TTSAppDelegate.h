//
//  TTSAppDelegate.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 2/5/18
//  Copyright (c) 2018 MOLO17. All rights reserved.
//

@interface TTSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
