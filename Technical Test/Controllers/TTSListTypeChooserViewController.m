//
//  TTSListTypeChooserViewController.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import "TTSListTypeChooserViewController.h"
#import <PureLayout/PureLayout.h>

@interface TTSListTypeChooserViewController ()

/**
 The button that allows users to display users in a list using a table view.
 */
@property (strong, nonnull, nonatomic) UIButton *tableListButton;

/**
 The button that allows users to display users in a grid.
 */
@property (strong, nonnull, nonatomic) UIButton *gridButton;

@end

@implementation TTSListTypeChooserViewController

#pragma mark - Init

- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (!self) {
        return nil;
    }
    
    self.tableListButton = [self makeButton];
    [self.tableListButton addTarget:self
                             action:@selector(didTapOnTableListButton)
                   forControlEvents:UIControlEventTouchUpInside];
    [self.tableListButton setTitle:NSLocalizedString(@"LIST", @"")
                          forState:UIControlStateNormal];
    
    self.gridButton = [self makeButton];
    [self.gridButton addTarget:self
                        action:@selector(didTapOnGridButton)
              forControlEvents:UIControlEventTouchUpInside];
    [self.gridButton setTitle:NSLocalizedString(@"GRID", @"")
                     forState:UIControlStateNormal];
    
    return self;
    
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE {
    return nil;
}

#pragma mark - View lifecycle

- (void)loadView {
    self.view = [[UIView alloc] init];
    
    [self.view addSubview:self.tableListButton];
    [self.view addSubview:self.gridButton];
    
    [self.tableListButton autoCenterInSuperview];
    
    [self.gridButton autoPinEdge:ALEdgeTop
                          toEdge:ALEdgeBottom
                          ofView:self.tableListButton
                      withOffset:16];
    [self.gridButton autoAlignAxisToSuperviewAxis:ALAxisVertical];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
}

#pragma mark - UI actions

- (void)didTapOnTableListButton {
    if (self.didChoose) {
        self.didChoose(TableList);
    }
}

- (void)didTapOnGridButton {
    if (self.didChoose) {
        self.didChoose(Grid);
    }
}

#pragma mark - Factories

- (UIButton *)makeButton {
    UIButton *b = [UIButton newAutoLayoutView];
    [b setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    return b;
}

@end
