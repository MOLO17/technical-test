//
//  TTSAppDelegate.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 2/5/18
//  Copyright (c) 2018 MOLO17. All rights reserved.
//

#import "TTSAppDelegate.h"
#import "TTSRootFlowCoordinator.h"
#import <AFNetworking/AFNetworking.h>

@interface TTSAppDelegate ()

@property (strong, nonatomic) TTSUserService * userService;
@property (strong, nonatomic) TTSRootFlowCoordinator * rootFlowCoordinator;

@end

@implementation TTSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window = [[UIWindow alloc] init];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    self.userService = [[TTSUserService alloc] initWithSessionManager:manager
                                                    requestSerializer:[AFHTTPRequestSerializer serializer]
                                                           userMapper:[TTSUserMapper sharedInstance]];
    
    self.rootFlowCoordinator = [[TTSRootFlowCoordinator alloc] initWithUserService:self.userService];
    [self.rootFlowCoordinator start];
    
    self.window.rootViewController = self.rootFlowCoordinator.initialViewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
