//
//  TTSUserListCollectionViewController.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 06/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PureLayout/PureLayout.h>
#import "TTSUserListCollectionViewController.h"
#import "TTSUserCollectionViewCell.h"
#import "TTSUserPresenter.h"
#import "TTSUserListPresenter+UIKit.h"

static NSString * const kUserListReuseIdentifier = @"com.molo17.TTSUserListCollectionViewCellReuseIdentifier";

@interface TTSUserListCollectionViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

/**
 The presenter that drives the view controller.
 */
@property (strong, nonnull, nonatomic) TTSUserListPresenter * presenter;

@property (strong, nonnull, nonatomic) UICollectionViewFlowLayout *layout;
/**
 The table view used to display the list of users.
 */
@property (strong, nonnull, nonatomic) UICollectionView * collectionView;

@end

@implementation TTSUserListCollectionViewController

#pragma mark - Init

- (instancetype)initWithPresenter:(TTSUserListPresenter *)presenter {
    self = [super initWithNibName:nil bundle:nil];
    if (!self) {
        return nil;
    }
    
    _presenter = presenter;
    _layout = [self makeCollectionViewFlowLayout];
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_layout];
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE {
    return nil;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE {
    return nil;
}

#pragma mark - View lifecycle

-(void)loadView {
    self.view = [[UIView alloc] init];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView autoPinEdgesToSuperviewEdges];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    [self.collectionView registerClass:[TTSUserCollectionViewCell class]
            forCellWithReuseIdentifier:kUserListReuseIdentifier];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    self.presenter.view = self;
}

#pragma mark - TTSUserListPresenterTargetView

- (void)displayUsers:(NSArray<TTSUser *> *)users {
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return (NSInteger) self.presenter.users.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                           cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TTSUserCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kUserListReuseIdentifier
                                                                                 forIndexPath:indexPath];

    TTSUser *user = [self.presenter userAtRow:indexPath];
    
    if (user) {
        cell.presenter = [[TTSUserPresenter alloc] initWithUser:user
                                                      imageKind:Medium
                                                    userService:self.presenter.service];
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TTSUser *user = [self.presenter userAtRow:indexPath];
    
    if (user && self.didSelectUser) {
        self.didSelectUser(user);
    }
}

#pragma mark - Factories

- (UICollectionViewFlowLayout *)makeCollectionViewFlowLayout {
    UICollectionViewFlowLayout *l = [[UICollectionViewFlowLayout alloc] init];
    
    CGFloat width = [[UIScreen mainScreen] bounds].size.width / 2;
    l.itemSize = CGSizeMake(width, width);
    l.minimumInteritemSpacing = 0;
    l.minimumLineSpacing = 16;
    
    return l;
}

@end
