//
//  TTSUserListTableViewController.m
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 05/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <PureLayout/PureLayout.h>
#import "TTSUserListTableViewController.h"
#import "TTSUserTableViewCell.h"
#import "TTSUserPresenter.h"
#import "TTSUserListPresenter+UIKit.h"

static NSString * const kUserListReuseIdentifier = @"com.molo17.TTSUserListTableViewCellReuseIdentifier";

@interface TTSUserListTableViewController () <UITableViewDataSource, UITableViewDelegate>

/**
 The presenter that drives the view controller.
 */
@property (strong, nonnull, nonatomic) TTSUserListPresenter * presenter;

/**
 The table view used to display the list of users.
 */
@property (strong, nonnull, nonatomic) UITableView * tableView;

@end

@implementation TTSUserListTableViewController

#pragma mark - Init

- (instancetype)initWithPresenter:(TTSUserListPresenter *)presenter {
    self = [super initWithNibName:nil bundle:nil];
    if (!self) {
        return nil;
    }
    
    _presenter = presenter;
    _tableView = [UITableView newAutoLayoutView];
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE {
    return nil;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE {
    return nil;
}

#pragma mark - View lifecycle

-(void)loadView {
    self.view = [[UIView alloc] init];
    
    [self.view addSubview:self.tableView];
    [self.tableView autoPinEdgesToSuperviewEdges];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerClass:[TTSUserTableViewCell class]
           forCellReuseIdentifier:kUserListReuseIdentifier];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.presenter.view = self;
}

#pragma mark - TTSUserListPresenterTargetView

- (void)displayUsers:(NSArray<TTSUser *> *)users {
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return (NSInteger) self.presenter.users.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TTSUserTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kUserListReuseIdentifier
                                                             forIndexPath:indexPath];
    TTSUser *user = [self.presenter userAtRow:indexPath];
    
    if (user) {
        cell.presenter = [[TTSUserPresenter alloc] initWithUser:user
                                                      imageKind:Thumbnail
                                                    userService:self.presenter.service];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TTSUser *user = [self.presenter userAtRow:indexPath];
    
    if (user && self.didSelectUser) {
        self.didSelectUser(user);
    }
}

@end
