//
//  TTSUserListCollectionViewController.h
//  Technical Test
//
//  Created by Alessandro Vendruscolo on 06/02/2018.
//  Copyright © 2018 MOLO17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSUser.h"
#import "TTSUserListPresenter.h"

/**
 The TTSUserListCollectionViewController displays a list of users using a
 Colelction View, providing an advanced way to list users, showing a bit more
 information about them.
 You can get notified of user actions using the various blocks. The most
 importantant one is `didSelectUser`.
 */
@interface TTSUserListCollectionViewController : UIViewController<TTSUserListPresenterTargetView>

/**
 A block that gets called when users select an user from the list.
 */
@property (copy, nullable, nonatomic) void (^didSelectUser)(TTSUser * _Nonnull);

/**
 Initializes a new view controller.
 
 @param presenter The presenter the view controller should use.
 @return The instantiated view controller.
 */
- (instancetype _Nullable)initWithPresenter:(TTSUserListPresenter * _Nonnull)presenter NS_DESIGNATED_INITIALIZER;

@end
