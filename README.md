# Technical Test #

## Requirements ##

* `bundler` gem should be installed and in your path (`[sudo] gem install bundler`).
* Xcode

## Setup ##

Run `bin/setup`

This will:

 - Install the gem dependencies
 - Install the pod dependencies

## Testing ##

Run `bin/test`

This will run the tests from the command line, and pipe the result through
[XCPretty][].

## Overview ##

The project makes use of these design patterns:

* Flow Coordinator
* MVP

[XCPretty]: https://github.com/supermarin/xcpretty
